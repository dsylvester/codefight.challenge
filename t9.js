function teeNine(message) {
    // CAT
    // convert Cat to number
    var myMap = MappedValues();
    var phoneCodes = MappedPhoneCodes();
    // let ces = splitBySpaces(message);
    var wordArray = (containsSpaces(message)) ? splitBySpaces(message) : [message];
    // Process each word in word Array
    console.log("WordArray: " + wordArray);
    var codes = wordArray.map(function (x) {
        return getT9Code(x, myMap);
    });
    var result = readNumbers(codes, phoneCodes);
    console.log("Codes: " + result);
    return 'bt';
}
function MappedValues() {
    var myMap = new Map();
    myMap.set('a', 2);
    myMap.set('b', 2);
    myMap.set('c', 2);
    myMap.set('d', 3);
    myMap.set('e', 3);
    myMap.set('f', 3);
    myMap.set('g', 4);
    myMap.set('h', 4);
    myMap.set('i', 4);
    myMap.set('j', 5);
    myMap.set('k', 5);
    myMap.set('l', 5);
    myMap.set('m', 6);
    myMap.set('n', 6);
    myMap.set('o', 6);
    myMap.set('p', 7);
    myMap.set('q', 7);
    myMap.set('r', 7);
    myMap.set('s', 7);
    myMap.set('t', 8);
    myMap.set('u', 8);
    myMap.set('v', 8);
    myMap.set('w', 9);
    myMap.set('x', 9);
    myMap.set('y', 9);
    myMap.set('z', 9);
    return myMap;
}
function MappedPhoneCodes() {
    var result = new Map();
    result.set("2", "a");
    result.set("22", "b");
    result.set("222", "c");
    result.set("3", "d");
    result.set("33", "e");
    result.set("333", "f");
    result.set("4", "g");
    result.set("44", "h");
    result.set("444", "i");
    result.set("5", "j");
    result.set("55", "k");
    result.set("555", "l");
    result.set("6", "m");
    result.set("66", "n");
    result.set("666", "o");
    result.set("7", "p");
    result.set("77", "q");
    result.set("777", "r");
    result.set("7777", "s");
    result.set("8", "t");
    result.set("88", "u");
    result.set("888", "v");
    result.set("9", "w");
    result.set("99", "x");
    result.set("999", "y");
    result.set("9999", "z");
    return result;
}
function containsSpaces(message) {
    console.log("Contains Spaces: " + message.includes(" "));
    return message.includes(" ");
}
function splitBySpaces(message) {
    console.log("message: " + message);
    return message.split(' ');
}
function getT9Code(message, myMap) {
    var result = "";
    message.toLowerCase().split('').forEach(function (x) {
        var val = myMap.get(x);
        console.log(x + " --> value --> " + val);
        result = "" + result + val;
    });
    return result;
}
function readNumber(code, answers) {
    //227
    var currentCode = code.split('');
    // let currentCode = parseInt(code);
    // 3
    var codeLength = code.length;
    // read the first digit 
    // 2 
    var tempVal = null;
    // 22
    var runningVal = "";
    var tempResult = "";
    for (var x = 0; x < codeLength; x++) {
        if (tempVal === null) {
            tempVal = currentCode[x];
            runningVal = currentCode[x];
        }
        else {
            // Check to see if the current digit 
            // is equal to tempVal
            if (tempVal == currentCode[x]) {
                // Append digit to running Val
                runningVal = "" + runningVal + currentCode[x];
            }
            else {
                // Get the value of runningVal
                var aa = tempResult = "" + tempResult + getKeyCode(runningVal, answers);
            }
        }
    }
    console.log("TempResult: " + tempResult);
    return tempResult;
}
function readNumbers(code, answers) {
    var result = new Array();
    code.forEach(function (x) {
        result.push(readNumber(x, answers));
    });
    return result;
}
function processCurrentDigit(num) {
    switch (num) {
        case 7:
            break;
        case 9:
            break;
        default:
            break;
    }
}
function getKeyCode(num, answers) {
    // check to see the key exist
    if (answers.has(num)) {
        return answers.get(num);
    }
    return num.toString();
}
